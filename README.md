This project generates a named RDF graph (and its metadata) where each annotation term from an ontology is associated with the number of elements refering to it, their frequency and its information content.

# Todo

- [ ] total number of elements in the corpus
- [ ] total number of direct annotations
- [ ] total number of direct + indirect annotations
- for each (direct+indirect) annotation:
    - [ ] number of corpus elements annotated 
    - [ ] proportion of corpus elements annotated
    - [ ] information content
- [ ] use query templates
- [ ] creation of a named graph
- metadata
    - [ ] reference to the corpus (location, version,...)
    - [ ] reference to the annotation ontology (location, version,...)
    - FAIR metrics
