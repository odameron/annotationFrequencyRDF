#! /usr/bin/env python3

import math
from SPARQLWrapper import SPARQLWrapper, JSON

endpointURL = 'http://localhost:3030/apid/query'

def readQueryFromFile(queryFilePath):
    query = ""
    with open(queryFilePath) as queryFile:
        query = queryFile.read()
    return query

sparql = SPARQLWrapper(endpointURL)
sparql.setQuery(readQueryFromFile('queries/query-annotationMetrics.rq'))
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

nbCorpusElts = 0
nbAnnotationsDirect = 0
nbAnnotationsTotal = 0
for result in results["results"]["bindings"]:
    #print(result["label"]["value"])
    nbCorpusElts = int(result['nbCorpusElts']["value"])
    nbAnnotationsDirect = int(result['nbAnnotationsDirect']["value"])
    nbAnnotationsTotal = result['nbAnnotationsTotal']["value"]

print('nbCorpusElts:\t' + str(nbCorpusElts))
print('nbAnnotationsDirect:\t' + str(nbAnnotationsDirect))
print('nbAnnotationsTotal:\t' + str(nbAnnotationsTotal))

print()

sparql.setQuery(readQueryFromFile('queries/query-nbAnnotatedElements.rq'))
sparql.setReturnFormat(JSON)
results = sparql.query().convert()
for result in results["results"]["bindings"]:
    #print(result['annotation']["value"] + '\t' + result['nbCorpusElts']["value"])
    print("<{}> :nbElementsAnnotated \"{}\"^^xsd:integer .".format(result['annotation']["value"], result['nbCorpusElts']["value"]))
    print("<{}> :proportionElementsAnnotated \"{}\"^^xsd:float .".format(result['annotation']["value"], (float(result['nbCorpusElts']["value"])/nbCorpusElts)))
    print("<{}> :informationContent \"{}\"^^xsd:float .".format(result['annotation']["value"], -math.log((float(result['nbCorpusElts']["value"])/nbCorpusElts))))
